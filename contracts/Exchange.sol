
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@chainlink/contracts/src/v0.8/interfaces/AggregatorV3Interface.sol";

contract Exchange {

    struct Token {
        address tokenContract;
        string symbolName;

        uint256 currentPrice;
    }

    mapping (address => uint8) public ethBalances;
    mapping (uint256 => Token) public allowedTokens;
    uint8 public tokensIndex;
    mapping (address => mapping (uint8 => uint256)) addressTokenBalances;
    mapping (address => address) tokenPriceFeedMapping;

    event DepositMade();
    event WithdrawMade();
    event TokenAdded(address _tokenContract, string _tokenName);

    receive() external payable {
        //
    }

    function viewTokenBalance(uint8 symbolNameIndex) public view returns (uint256) {
        return addressTokenBalances[msg.sender][symbolNameIndex];
    }


    function addTokenToExchange(address tokenContract, string memory _tokenName, address priceFeed) public {
        tokensIndex ++;
        allowedTokens[tokensIndex].tokenContract = tokenContract;
        allowedTokens[tokensIndex].symbolName = _tokenName;
        tokenPriceFeedMapping[tokenContract] = priceFeed;

        emit TokenAdded(tokenContract, _tokenName);
    }

    function deposit (string memory symbolName, uint256 _value) public {
        uint8 symbolNameIndex = getSymbolIndex(symbolName);
        // check if token is allowed
        require(tokenInExchange(symbolName));

        ERC20 token = ERC20(allowedTokens[symbolNameIndex].tokenContract);
        // add to balances
        token.transferFrom(msg.sender, address(this), _value);
        addressTokenBalances[msg.sender][symbolNameIndex] += _value;

        emit DepositMade();
    }

    function withdraw(string memory symbolName, uint256 _value) public  {
        // check if balance is available
        uint8 symbolNameIndex = getSymbolIndex(symbolName);
        uint256 userBalance = addressTokenBalances[msg.sender][symbolNameIndex];
        require(userBalance >= _value, "not enough balance");
        ERC20 token = ERC20(allowedTokens[symbolNameIndex].tokenContract);
        token.transfer(msg.sender, addressTokenBalances[msg.sender][symbolNameIndex]);

        // update user balance
        addressTokenBalances[msg.sender][symbolNameIndex] -= _value;
        emit WithdrawMade();
    }
    function tokenInExchange(string memory symbolName) internal returns (bool) {
        uint8 index = getSymbolIndex(symbolName);
        if (index == 0) {
            return false;
        }
        return true;
    }

    function getSymbolIndex(string memory symbolName) internal returns (uint8) {
        for (uint8 i = 1; i <= tokensIndex; i++) {
            if (stringsEqual(allowedTokens[i].symbolName, symbolName)) {
                return i;
            }
        }
        return 0;
    }

    function stringsEqual(string memory _a, string memory _b) internal returns (bool) {
        return (keccak256(abi.encodePacked((_a))) == keccak256(abi.encodePacked((_b))));
    }

    function exchange(string memory tokenASymbol, uint256 tokenAAmount, string memory tokenBSymbol) public {
        // both tokens must be supported by the Exchange
        require(tokenInExchange(tokenASymbol), "tokenA not in exchange");
        require(tokenInExchange(tokenBSymbol), "tokenB not in exchange");

        // check if the caller has enough TokenA balance
        uint8 tokenASymbolIndex = getSymbolIndex(tokenASymbol);
        uint8 tokenBSymbolIndex = getSymbolIndex(tokenBSymbol);
        uint256 userTokenABalance = addressTokenBalances[msg.sender][tokenASymbolIndex];
        require(userTokenABalance >= tokenAAmount, "not enough balance");

        // check if exchange owns enough tokenB
        uint256 tokenAPriceInWei = getTokenEthPrice(allowedTokens[tokenASymbolIndex].tokenContract);
        uint256 tokenBPriceInWei = getTokenEthPrice(allowedTokens[tokenBSymbolIndex].tokenContract);
        uint256 tokenBAmount = (tokenAAmount * tokenBPriceInWei) / tokenAPriceInWei;
        uint256 tokenBBalance = ERC20(allowedTokens[tokenBSymbolIndex].tokenContract).balanceOf(address(this));
        require(tokenBBalance >= tokenBAmount, "not enough tokenB in exchange");

        // now that all is Ok lets do it
        addressTokenBalances[msg.sender][tokenASymbolIndex] -= tokenAAmount;
        addressTokenBalances[msg.sender][tokenBSymbolIndex] += tokenBAmount;
    }

    function getExchangeRate(address tokenASymbol, address tokenBSymbol) public view returns (uint256){
        // assuming we are not making anything from this exchange! wtf then lol
        uint256 tokenAPrice = getTokenEthPrice(tokenASymbol);
        uint256 tokenBPrice = getTokenEthPrice(tokenBSymbol);
        return tokenAPrice / tokenBPrice;
    }

    function getTokenEthPrice(address tokenContract) public view returns (uint256) {
        address priceFeedAddress = tokenPriceFeedMapping[tokenContract];
        AggregatorV3Interface priceFeed = AggregatorV3Interface(
            priceFeedAddress
        );
        (
            uint80 roundID,
            int256 price,
            uint256 startedAt,
            uint256 timeStamp,
            uint80 answeredInRound
        ) = priceFeed.latestRoundData();
        return uint256(price);
    }
}

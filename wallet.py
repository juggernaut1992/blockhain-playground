

import json

from uniswap import Uniswap
from web3 import Web3
from eth_account import Account
import secrets
from base64 import urlsafe_b64encode as b64e, urlsafe_b64decode as b64d

from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

backend = default_backend()


class Token:

    def __init__(self, symbol_name, abi, contract_address):
        self.symbol_name = symbol_name
        self.abi = abi
        self.contract_address = contract_address


class Wallet:

    def __init__(
             self,
             eth_node_http_server,
             passphrase,
             private_key=None,
             new=False,
             init_uniswap=False,
             uniswap_version=None
    ):
        self.tokens = self._load_tokens()
        self._generate_account(passphrase, private_key, new=new)
        self.locked = False
        self.is_secure = False
        self.eth_node_http_server = eth_node_http_server
        self.w3 = Web3(Web3.HTTPProvider(eth_node_http_server))
        self.uniswap = None
        if init_uniswap:
            self.uniswap = Uniswap(
                address=self.get_wallet_address(),
                private_key=private_key,
                version=uniswap_version,
                provider=self.eth_node_http_server
            )

    def uniswap_swap_token(self, token_a, token_b, amount):
        self.uniswap.make_trade(token_a, token_b, amount)

    def _load_tokens(self):
        tokens = {}
        with open('tokens.json', 'r') as coins_file:
            all_coins = json.load(coins_file).get('tokens')
            for a in all_coins:
                tokens[a.get('symbolName')] = Token(a.get('symbolName'), a.get('abi'), a.get('contractAddress'))
        return tokens

    def _store_private_key_securely(self, passphrase, private_key):
        with open('wallet.data', 'w') as w:
            w.write(str(self.encode_private_key(bytes(private_key), passphrase)))

    def _load_private_key(self, passphrase):
        with open('wallet.data', 'r') as w:
            data = w.read()
            return self.decode_private_key(bytes(data), passphrase)

    def _generate_account(self, passphrase, private_key=None, new=False):
        if private_key:
            self.account = Account.from_key(private_key)
        elif not private_key and new:
            self.account = Account.from_key("0x" + secrets.token_hex(32))
            self._store_private_key_securely(passphrase, private_key)
        elif not private_key and not new:
            return self._load_private_key(passphrase)

    def token_in_wallet(self, symbol_name):
        return symbol_name in self.tokens.keys()

    def add_token_to_wallet(self, token):
        self.tokens[token.symbol_name] = token

    def get_wallet_address(self):
        return self.account.address

    def get_eth_balance(self):
        return self.account.balance

    def send_eth(self, to, value, passphrase, gas=100000):
        signed_txn = self.w3.eth.account.signTransaction(dict(
            nonce=self.w3.eth.getTransactionCount(self.get_wallet_address()),
            gasPrice=self.w3.eth.gasPrice,
            gas=gas,
            to=to,
            value=Web3.toWei(value, 'ether')
        ),
            self._load_private_key(passphrase))

        return self.w3.eth.sendRawTransaction(signed_txn.rawTransaction)

    def get_balance_for_token(self, token, address):
        contract = self.w3.eth.contract(Web3.toChecksumAddress(token.contract_address), abi=token.abi)
        return contract.functions.balanceOf(address).call()

    def get_transaction(self, tx_hash):
        return self.w3.eth.getTransaction(tx_hash)

    def get_transactions_history(self, address, last=15, offset=0):
        transactions = []
        last_block = self.w3.eth.block_number
        for i in range(last_block, last_block - last - offset):
            block = self.w3.eth.get_block(i, True)
            if block and block.transactions:
                for transaction in block.transactions:
                    if transaction.get('from') == address \
                            or transaction.get('to') == address:
                        transactions.append(transaction)
        return transactions

    def transfer(self, symbol_name, value, receiver):
        if not self.token_in_wallet(symbol_name):
            return
        token_contract = self._build_contract(self.tokens.get(symbol_name))
        token_contract.transfer(receiver, value, {"from": self.account})

    def _build_contract(self, token):
        return self.w3.eth.contract(token.contract_address, abi=token.abi)

    def encode_private_key(self, private_key, password, iterations=100_000):
        salt = secrets.token_bytes(16)
        key = self._derive_key(password.encode(), salt, iterations)
        return b64e(
            b'%b%b%b' % (
                salt,
                iterations.to_bytes(4, 'big'),
                b64d(Fernet(key).encrypt(private_key)),
            )
        )

    def decode_private_key(self, private_key, password):
        decoded = b64d(private_key)
        salt, iter, private_key = decoded[:16], decoded[16:20], b64e(decoded[20:])
        iterations = int.from_bytes(iter, 'big')
        key = self._derive_key(password.encode(), salt, iterations)
        return Fernet(key).decrypt(private_key)

    @staticmethod
    def _derive_key(password, salt, iterations=100_000):
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(), length=32, salt=salt,
            iterations=iterations, backend=backend)
        return b64e(kdf.derive(password))

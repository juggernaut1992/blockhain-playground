
from brownie import Exchange, accounts, network, config, MockUSDT, interface, MockZRX, MockV3Aggregator
from web3 import Web3
import pytest

DECIMALS = 8


def test_exchange():
    exchange_contract = Exchange.deploy({"from": accounts[0]})

    usdt_mock = MockUSDT.deploy({"from": accounts[0]})
    usdt_mock.mint(Web3.toWei(100, "ether"))
    usdt_mock.transfer(accounts[0].address, Web3.toWei(2, "ether"))

    zrx_mock = MockZRX.deploy({"from": accounts[0]})
    zrx_mock.mint(Web3.toWei(100, "ether"))
    zrx_mock.transfer(accounts[0].address, Web3.toWei(2, "ether"))

    usdt_price_feed_mock = MockV3Aggregator.deploy(DECIMALS, 20000, {"from": accounts[0]})
    zrx_price_feed_mock = MockV3Aggregator.deploy(DECIMALS, 40000, {"from": accounts[0]})

    print("Send some eth to the contract")
    # add tokens to the exchange
    tokens_to_add = [
        {
            "symbol": "USDT",
            "contractAddress": usdt_mock.address,
            "priceFeed": usdt_price_feed_mock.address
        },
        {
            "symbol": "ZRX",
            "contractAddress": zrx_mock.address,
            "priceFeed": zrx_price_feed_mock.address
        }
    ]
    for token in tokens_to_add:
        exchange_contract.addTokenToExchange(
            token.get("contractAddress"),
            token.get("symbol"),
            token.get("priceFeed"),
            {"from": accounts[0]}
        )
    zrx_contract = interface.IERC20(zrx_mock.address)
    usdt_contract = interface.IERC20(usdt_mock.address)

    # lets transfer USDT to the exchange so we have something as tokenB
    usdt_contract.transfer(exchange_contract.address, Web3.toWei(1, "ether"), {"from": accounts[0]})
    #
    # approve the transaction that account 0 is gonna deposit 1 LINK to the exchange.
    zrx_contract.approve(exchange_contract.address, Web3.toWei(1, "ether"), {"from": accounts[0]})

    print("Deposit ZRX")
    exchange_contract.deposit("ZRX", Web3.toWei(1, "ether"), {"from": accounts[0]})
    assert exchange_contract.viewTokenBalance(2, {"from": accounts[0]}) == Web3.toWei(1, "ether")

    print("Exchange ZRX for USDT!")
    exchange_contract.exchange("ZRX", Web3.toWei(1, "ether"), "USDT", {"from": accounts[0]})
    assert exchange_contract.viewTokenBalance(2, {"from": accounts[0]}) == 0
    assert exchange_contract.viewTokenBalance(1, {"from": accounts[0]}) == Web3.toWei(0.5, "ether")

    print("Withdraw the USDT")
    exchange_contract.withdraw("USDT", Web3.toWei(0.5, "ether"), {"from": accounts[0]})
    assert exchange_contract.viewTokenBalance(1, {"from": accounts[0]}) == 0
    print("Done")

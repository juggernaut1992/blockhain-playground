// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


import "./MintableERC20.sol";


contract MockZRX is MintableERC20 {
    string public symbol_ = "ZRX";
    string public name_ = "0x Protocol Token";

    constructor () MintableERC20(name_, symbol_) {}

}
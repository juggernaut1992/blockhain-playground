// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


import "./MintableERC20.sol";


contract MockUSDT is MintableERC20 {
    string public symbol_ = "USDT";
    string public name_ = "USDT Coin";

    constructor () MintableERC20(name_, symbol_) {}

}